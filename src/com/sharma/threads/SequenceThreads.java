package com.sharma.threads;

public class SequenceThreads {

	public int flag = 1;

	public static void main(String[] args) {
		SequenceThreads obj = new SequenceThreads();
		ThreadA t1 = new ThreadA(obj);
		ThreadB t2 = new ThreadB(obj);
		ThreadC t3 = new ThreadC(obj);

		t1.start();
		t2.start();
		t3.start();
	}
}

class ThreadA extends Thread {

	private SequenceThreads obj;

	public ThreadA(SequenceThreads obj) {
		this.obj = obj;
	}

	@Override
	public void run() {
		try {
			synchronized (obj) {
				while (obj.flag != 1) {
					obj.wait();
				}
				System.out.println("Inside Thread A");
				Thread.sleep(3000);
				System.out.println("Completed Thread A !");
				obj.flag = 2;
				obj.notifyAll();
			}
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}
}

class ThreadB extends Thread {

	private SequenceThreads obj;

	public ThreadB(SequenceThreads obj) {
		this.obj = obj;
	}

	@Override
	public void run() {
		try {
			synchronized (obj) {
				while (obj.flag != 2) {
					obj.wait();
				}
				System.out.println("Inside Thread B");
				Thread.sleep(3000);
				System.out.println("Completed Thread B !");
				obj.flag = 3;
				obj.notifyAll();
			}
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}
}

class ThreadC extends Thread {

	private SequenceThreads obj;

	public ThreadC(SequenceThreads obj) {
		this.obj = obj;
	}

	@Override
	public void run() {
		try {
			synchronized (obj) {
				while (obj.flag != 3) {
					obj.wait();
				}
				System.out.println("Inside Thread C");
				Thread.sleep(3000);
				System.out.println("Completed Thread C !");
				obj.flag = 1;
				obj.notifyAll();
			}
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}
}
