package com.sharma.imageprocessing;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Cartoonify {

	private static final double SIGMA = 1.5;
	private static final int KERNEL_SIZE = 3; // MUST BE AN ODD INTEGER

	public static void main(String[] args) {
		/*grayscale();
		getKernel();*/
		colorDodgeBlendMerge();
	}

	public static void grayscale() {
		File originalImage = new File("/home/deepak/Desktop/images/face.jpg");
		File grayscaleImage = new File("/home/deepak/Desktop/images/face_G.jpg");

		BufferedImage image;
		BufferedImage newImage;
		int height, width;

		try {
			image = ImageIO.read(originalImage);
			height = image.getHeight();
			width = image.getWidth();
			newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					Color c = new Color(image.getRGB(i, j));
					int r, g, b;
					r = 255 - (c.getRed() + c.getGreen() + c.getBlue()) / 3;
					g = 255 - (c.getRed() + c.getGreen() + c.getBlue()) / 3;
					b = 255 - (c.getRed() + c.getGreen() + c.getBlue()) / 3;

					Color c1 = new Color(r, g, b);
					newImage.setRGB(i, j, c1.getRGB());
				}
			}
			ImageIO.write(newImage, "jpg", grayscaleImage);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void getKernel() {
		double[][] kernel = new double[KERNEL_SIZE][KERNEL_SIZE];
		double[][] gaussianMatrix = new double[KERNEL_SIZE][KERNEL_SIZE];
		double sum = 0;

		File originalImage = new File("/home/deepak/Desktop/images/face_G.jpg");
		File gaussianBlurImage = new File("/home/deepak/Desktop/images/face_GB.jpg");
		BufferedImage image;
		BufferedImage gaussImage;
		int height, width;

		int p = -(KERNEL_SIZE / 2);
		int q = KERNEL_SIZE / 2;
		double s = 2 * SIGMA * SIGMA;
		for (int i = 0; i < KERNEL_SIZE; i++) {
			for (int j = 0; j < KERNEL_SIZE; j++) {
				kernel[i][j] = (7 / (22 * s)) * Math.exp(-((p * p + q * q) / s));
				sum += kernel[i][j];
				p++;
			}
			q--;
		}
		// Normalize the kernel
		for (int i = 0; i < KERNEL_SIZE; i++) {
			for (int j = 0; j < KERNEL_SIZE; j++) {
				kernel[i][j] /= sum;
			}
		}

		try {
			image = ImageIO.read(originalImage);
			width = image.getWidth();
			height = image.getHeight();
			gaussImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

			for (int i = KERNEL_SIZE / 2; i < width - (KERNEL_SIZE / 2); i++) {
				for (int j = KERNEL_SIZE / 2; j < height - (KERNEL_SIZE / 2); j++) {
					int a = i - (KERNEL_SIZE / 2);
					int b = j - (KERNEL_SIZE / 2);
					double gaussSum = 0;
					for (int x = 0; x < KERNEL_SIZE && b < height - (KERNEL_SIZE / 2); x++) {
						for (int y = 0; y < KERNEL_SIZE && a < width - (KERNEL_SIZE / 2); y++) {
							gaussianMatrix[x][y] = kernel[x][y] * image.getRGB(a, b);
							gaussSum += gaussianMatrix[x][y];
							a++;
						}
						b++;
					}
					gaussImage.setRGB(i, j, (int) gaussSum);
				}
			}
			ImageIO.write(gaussImage, "jpg", gaussianBlurImage);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void colorDodgeBlendMerge() {
		File originalImage = new File("/home/deepak/Desktop/images/face.jpg");
		File gaussianBlurImage = new File("/home/deepak/Desktop/images/face_GB.jpg");
		File toonImage = new File("/home/deepak/Desktop/images/face_TOON.jpg");
		BufferedImage image;
		BufferedImage gaussImage;
		BufferedImage cartoonImage;
		int height, width;

		try {
			image = ImageIO.read(originalImage);
			gaussImage = ImageIO.read(gaussianBlurImage);

			height = image.getHeight();
			width = image.getWidth();
			cartoonImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					Color sourceColor = new Color(image.getRGB(i, j));
					Color layerColor = new Color(gaussImage.getRGB(i, j));
					int sourceRed, sourceGreen, sourceBlue;
					sourceRed = sourceColor.getRed();
					sourceGreen = sourceColor.getGreen();
					sourceBlue = sourceColor.getBlue();

					int layerRed, layerGreen, layerBlue;
					layerRed = layerColor.getRed();
					layerGreen = layerColor.getGreen();
					layerBlue = layerColor.getBlue();

					Color c1 = new Color(colordodge(layerRed, sourceRed), colordodge(layerGreen, sourceGreen),
							colordodge(layerBlue, sourceBlue));
					// Color c1 = new Color(colordodge(sourceRed,layerRed),
					// colordodge(sourceGreen,layerGreen), colordodge(sourceBlue,layerBlue));
					cartoonImage.setRGB(i, j, c1.getRGB());
				}
			}
			ImageIO.write(cartoonImage, "jpg", toonImage);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// in1-->layer, in2-->source
	private static int colordodge(int in1, int in2) {
		float image = (float) in2;
		float mask = (float) in1;
		return ((int) ((image == 255) ? image : Math.min(255, (((long) mask << 8) / (255 - image)))));
	}
}
