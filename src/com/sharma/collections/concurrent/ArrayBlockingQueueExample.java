package com.sharma.collections.concurrent;

import java.util.concurrent.ArrayBlockingQueue;

public class ArrayBlockingQueueExample {

	public static void main(String[] args) {

		ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<String>(10);

		Producer producer = new Producer(queue);
		Consumer consumer = new Consumer(queue);

		producer.start();
		consumer.start();
	}
}

class Producer extends Thread {

	ArrayBlockingQueue<String> queue;

	public Producer(ArrayBlockingQueue<String> queue) {
		this.queue = queue;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			try {
				System.out.println("Producing value " + i);
				queue.put("value " + i);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

class Consumer extends Thread {

	ArrayBlockingQueue<String> queue;

	public Consumer(ArrayBlockingQueue<String> queue) {
		this.queue = queue;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			try {
				// Thread.sleep(1000);
				System.out.println("Consuming " + queue.take());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
