package com.sharma.oop;

public class Overriding extends Parent {

	@Override
	public void printSum() {
		//return 1;
		System.out.println("Inside CHILD printSum");
	}
	
	public void print() {
		System.out.println("Inside CHILD print");
	}
	
	public static void main(String[] args) {
		Parent p = new Overriding();
		p.printSum();
	}
}

class Parent {
	public void printSum() {
		System.out.println("Inside PARENT printSum");
	}
}